﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraRotate : MonoBehaviour
{

    public float lookUpSpeed = 1.0f;
    public float lookAroundSpeed = 2.0f;
    public float viewClamp;
    public float negViewClamp;

    private float LR = 0.0f;
    private float UD = 0.0f;

	void Start()
	{
        Cursor.visible = true;	
	}

	void Update()
    {
        //camera rotation
        LR += lookUpSpeed * Input.GetAxis("Mouse X");
        UD -= lookAroundSpeed * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(UD, LR, 0.0f);

        //updown look clamp
        if (UD > viewClamp)
        {
            transform.localEulerAngles = new Vector3(viewClamp, 0, 0);
        }
        else
        {
            if (UD < negViewClamp)
            {
                transform.localEulerAngles = new Vector3(negViewClamp, 0, 0);
            }
        }
   
    }
}