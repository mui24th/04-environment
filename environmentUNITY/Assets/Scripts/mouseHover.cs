﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class mouseHover : MonoBehaviour
{

    //misc
    public Camera mainCamera;
    public float transitionSpeed = 100.0f;

    //sounds
    public AudioSource newArea;
    public AudioSource click;

    //interactable objects
    public GameObject openBook1;
    public GameObject openBook2;
    public GameObject openBook3;
    public GameObject openBook4;
    public GameObject openBook5;
    public GameObject spirit;
    public GameObject spiritOne;
    public GameObject hourGlass;
    public GameObject tree1;
    public GameObject tree2;
    public GameObject tree3;
    public GameObject treeInside;
    public GameObject archiveTower;
    public GameObject globe;
    public GameObject quillInk;
    public GameObject poster3;
    public GameObject shelves;
    public GameObject chair;
    public GameObject photoFrames;


    //teleports
    public GameObject BC;
    public GameObject AD;
    public GameObject arrowUp;
    public GameObject arrowDown;
    public GameObject arrowToRoom;
    public GameObject arrowToExterior;
    public GameObject arrowToBC;
    public GameObject arrowToBalcony;
    public GameObject arrowToRoomFB;
    public GameObject arrowToLobby;
    public GameObject arrowToExteriorFR;


    //locations
    Vector3 BCpos;
    Vector3 ADpos;
    Vector3 lobbyPos;
    Vector3 exteriorPos;
    Vector3 roomPos;
    Vector3 balconyPos;

    //textboxes
    public GameObject spiritOnetextBox1;
    public GameObject spiritOnetextBox2;
    public GameObject spiritOnetextBox3;
    public GameObject spiritOnetextBox4;
    public GameObject spiritOnetextBox5;

    public GameObject spiritTwotextBox1;
    public GameObject spiritTwotextBox2;
    public GameObject spiritTwotextBox3;
    public GameObject spiritTwotextBox4;
    public GameObject spiritTwotextBox5;
    public GameObject spiritTwotextBox6;
    public GameObject spiritTwotextBox7;

    public GameObject book1textBox;
    public GameObject book2textBox;
    public GameObject book3textBox;
    public GameObject book4textBox;
    public GameObject book5textBox;

    public GameObject hourglassTextBox;
    public GameObject treeRootTextbox;
    public GameObject archiveTextbox;
    public GameObject globeTextbox;
    public GameObject inkTextbox;
    public GameObject atlasTextbox;
    public GameObject shelvesTextbox;
    public GameObject chairTextbox;
    public GameObject photoframesTextbox;



    void Start()
    {

        //referencing
        openBook1 = GameObject.Find("openBook1");
        openBook2 = GameObject.Find("openBook2");
        openBook3 = GameObject.Find("openBook3");
        openBook4 = GameObject.Find("openBook4");
        openBook5 = GameObject.Find("openBook5");
        spiritOne = GameObject.Find("spiritOne");
        spirit = GameObject.Find("spirit");
        hourGlass = GameObject.Find("hourGlass");
        tree1 = GameObject.Find("tree1");
        tree2 = GameObject.Find("tree2");
        tree3 = GameObject.Find("tree3");
        treeInside = GameObject.Find("treeInside");
        archiveTower = GameObject.Find("archiveArea11:arc41");
        globe = GameObject.Find("room7:pSphere6");
        quillInk = GameObject.Find("pCube5Ink");
        poster3 = GameObject.Find("poster3");
        shelves = GameObject.Find("room7:backshelf3");
        chair = GameObject.Find("chair:back2");
        photoFrames = GameObject.Find("pictureFrame:pictureFrame");


        //directional object ref
        BC = GameObject.Find("BC");
        AD = GameObject.Find("AD");
        arrowUp = GameObject.Find("arrowUp");
        arrowDown = GameObject.Find("arrowDown");
        arrowToRoom = GameObject.Find("arrowToRoom");
        arrowToExterior = GameObject.Find("arrowToExterior");
        arrowToBC = GameObject.Find("arrowToBC");
        arrowToBalcony = GameObject.Find("arrowToBalcony");
        arrowToRoomFB = GameObject.Find("arrowToRoomFB");
        arrowToLobby = GameObject.Find("arrowToLobby");
        arrowToExteriorFR = GameObject.Find("arrowToExteriorFR");


        //location ref
        BCpos = new Vector3(-4, 118, 97);
        ADpos = new Vector3(-4, 129, 97);
        lobbyPos = new Vector3(-13, 100, 104);
        exteriorPos = new Vector3(-12, 114, 99);
        balconyPos = new Vector3(3, 141, 92);
        roomPos = new Vector3(-3, 140, 102);


        //textbox bool

        spiritOnetextBox1.SetActive(false);
        spiritOnetextBox2.SetActive(false);
        spiritOnetextBox3.SetActive(false);
        spiritOnetextBox4.SetActive(false);
        spiritOnetextBox5.SetActive(false);

        spiritTwotextBox1.SetActive(false);
        spiritTwotextBox2.SetActive(false);
        spiritTwotextBox3.SetActive(false);
        spiritTwotextBox4.SetActive(false);
        spiritTwotextBox5.SetActive(false);
        spiritTwotextBox6.SetActive(false);
        spiritTwotextBox7.SetActive(false);

        book1textBox.SetActive(false);
        book2textBox.SetActive(false);
        book3textBox.SetActive(false);
        book4textBox.SetActive(false);
        book5textBox.SetActive(false);

        hourglassTextBox.SetActive(false);
        treeRootTextbox.SetActive(false);
        archiveTextbox.SetActive(false);
        globeTextbox.SetActive(false);
        inkTextbox.SetActive(false);
        atlasTextbox.SetActive(false);
        shelvesTextbox.SetActive(false);
        chairTextbox.SetActive(false);
        photoframesTextbox.SetActive(false);


        //halo
        hourGlass.GetComponent<Behaviour>().enabled = false;
        openBook1.GetComponent<Behaviour>().enabled = false;
        openBook2.GetComponent<Behaviour>().enabled = false; 
        openBook3.GetComponent<Behaviour>().enabled = false; 
        openBook4.GetComponent<Behaviour>().enabled = false; 
        openBook5.GetComponent<Behaviour>().enabled = false; 
        spiritOne.GetComponent<Behaviour>().enabled = false; 
        spirit.GetComponent<Behaviour>().enabled = false; 
        hourGlass.GetComponent<Behaviour>().enabled = false; 
        tree1.GetComponent<Behaviour>().enabled = false; 
        tree2.GetComponent<Behaviour>().enabled = false; 
        tree3.GetComponent<Behaviour>().enabled = false; 
        treeInside.GetComponent<Behaviour>().enabled = false; 
        archiveTower.GetComponent<Behaviour>().enabled = false; 
        globe.GetComponent<Behaviour>().enabled = false; 
        quillInk.GetComponent<Behaviour>().enabled = false; 
        poster3.GetComponent<Behaviour>().enabled = false; 
        shelves.GetComponent<Behaviour>().enabled = false; 
        chair.GetComponent<Behaviour>().enabled = false; 
        photoFrames.GetComponent<Behaviour>().enabled = false; 
        BC.GetComponent<Behaviour>().enabled = false; 
        AD.GetComponent<Behaviour>().enabled = false; 
        arrowUp.GetComponent<Behaviour>().enabled = false; 
        arrowDown.GetComponent<Behaviour>().enabled = false; 
        arrowToRoom.GetComponent<Behaviour>().enabled = false; 
        arrowToExterior.GetComponent<Behaviour>().enabled = false; 
        arrowToBC.GetComponent<Behaviour>().enabled = false; 
        arrowToBalcony.GetComponent<Behaviour>().enabled = false; 
        arrowToRoomFB.GetComponent<Behaviour>().enabled = false; 
        arrowToExteriorFR.GetComponent<Behaviour>().enabled = false; 
        arrowToLobby.GetComponent<Behaviour>().enabled = false; 



    }

    void Update()
    {

        //click detection
        RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            ///archive room
            if (hit.collider.gameObject.name == openBook1.name)
            {
                openBook1.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("book1Pressed");
                    book1textBox.SetActive(true);
                }
            }
            else
            {
                openBook1.GetComponent<Behaviour>().enabled = false;
            }

            if (hit.collider.gameObject.name == openBook2.name)
            {
                openBook2.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("book2Pressed");
                    book2textBox.SetActive(true);
                }
            }
            else
            {
                openBook2.GetComponent<Behaviour>().enabled = false; 
            }

            if (hit.collider.gameObject.name == openBook3.name)
            {
                openBook3.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("book3Pressed");
                    book3textBox.SetActive(true);
                }
            }
            else
            {
                openBook3.GetComponent<Behaviour>().enabled = false; 
            }

            if (hit.collider.gameObject.name == openBook4.name)
            {
                openBook4.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("book4Pressed");
                    book4textBox.SetActive(true);
                }
            }
            else
            {
                openBook4.GetComponent<Behaviour>().enabled = false; 
            }

            if (hit.collider.gameObject.name == openBook5.name)
            {
                openBook5.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("book5Pressed");
                    book5textBox.SetActive(true);
                }
            }
            else
            {
                openBook5.GetComponent<Behaviour>().enabled = false;
            }

            if (hit.collider.gameObject.name == BC.name)
            {
                BC.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("BCPressed");
                    //mainCamera.transform.position = BCpos;
                    mainCamera.transform.position = Vector3.Slerp(ADpos, BCpos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;

                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }
            }
            else
            {
                BC.GetComponent<Behaviour>().enabled = false; 
            }
            if (hit.collider.gameObject.name == arrowDown.name)
            {
                arrowDown.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    mainCamera.transform.position = Vector3.Slerp(ADpos, BCpos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;
                    //mainCamera.transform.position = BCpos;
                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }
            }
            else
            {
                arrowDown.GetComponent<Behaviour>().enabled = false; 
            }

            if (hit.collider.gameObject.name == AD.name)
            {
                AD.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    mainCamera.transform.position = Vector3.Slerp(BCpos, ADpos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;
                    //mainCamera.transform.position = ADpos;
                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }
            }
            else
            {
                AD.GetComponent<Behaviour>().enabled = false; 
            }
            if (hit.collider.gameObject.name == arrowUp.name)
            {
                arrowUp.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    mainCamera.transform.position = Vector3.Slerp(BCpos, ADpos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;
                    //mainCamera.transform.position = ADpos;
                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }

            }
            else
            {
                arrowUp.GetComponent<Behaviour>().enabled = false; 
            }
            if (hit.collider.gameObject.name == arrowToRoom.name)
            {
                arrowToRoom.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("arrowToRoomPressed");
                    mainCamera.transform.position = Vector3.Slerp(ADpos, roomPos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;
                    //mainCamera.transform.position = roomPos;
                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }
            }
            else
            {
                arrowToRoom.GetComponent<Behaviour>().enabled = false;
            }
            if (hit.collider.gameObject.name == archiveTower.name)
            {
                archiveTower.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    archiveTextbox.SetActive(true);

                }

            }
            else
            {
                archiveTower.GetComponent<Behaviour>().enabled = false;
            }
            //room
            if (hit.collider.gameObject.name == arrowToBalcony.name)
            {
                arrowToBalcony.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    mainCamera.transform.position = Vector3.Slerp(roomPos, balconyPos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;
                    //mainCamera.transform.position = balconyPos;
                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }
            }
            else
            {
                arrowToBalcony.GetComponent<Behaviour>().enabled = false; 
            }

            if (hit.collider.gameObject.name == arrowToExteriorFR.name)
            {
                arrowToExteriorFR.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    mainCamera.transform.position = Vector3.Slerp(roomPos, exteriorPos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;
                    //mainCamera.transform.position = balconyPos;
                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }
            }
            else
            {
                arrowToExteriorFR.GetComponent<Behaviour>().enabled = false; 
            }

            if (hit.collider.gameObject.name == arrowToLobby.name)
            {
                arrowToLobby.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    mainCamera.transform.position = Vector3.Slerp(exteriorPos, lobbyPos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;
                    //mainCamera.transform.position = balconyPos;
                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }
            }
            else
            {
                arrowToLobby.GetComponent<Behaviour>().enabled = false;
            }
       
            if (hit.collider.gameObject.name == globe.name)
            {
                globe.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("globePressed");
                    globeTextbox.SetActive(true);
                }
            }
            else
            {
                globe.GetComponent<Behaviour>().enabled = false;
            }
            if (hit.collider.gameObject.name == quillInk.name)
            {
                quillInk.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("quillInkPressed");
                    inkTextbox.SetActive(true);
                }
            }
            else
            {
                quillInk.GetComponent<Behaviour>().enabled = false;
            }
            if (hit.collider.gameObject.name == poster3.name)
            {
                poster3.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("atlasPressed");
                    atlasTextbox.SetActive(true);
                }
            }
            else
            {
                poster3.GetComponent<Behaviour>().enabled = false; 
            }
            if (hit.collider.gameObject.name == shelves.name)
            {
                shelves.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("shelvesPressed");
                    shelvesTextbox.SetActive(true);
                }
            }
            else
            {
                shelves.GetComponent<Behaviour>().enabled = false; 
            }
            if (hit.collider.gameObject.name == chair.name)
            {
                chair.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("chairPressed");
                    chairTextbox.SetActive(true);
                }
            }
            else
            {
                chair.GetComponent<Behaviour>().enabled = false; 
            }
            if (hit.collider.gameObject.name == photoFrames.name)
            {
                photoFrames.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("photosPressed");
                    photoframesTextbox.SetActive(true);
                }
            }
            else
            {
                photoFrames.GetComponent<Behaviour>().enabled = false; 
            }

            //balcony
            if (hit.collider.gameObject.name == arrowToRoomFB.name)
            {
                arrowToRoomFB.GetComponent<Behaviour>().enabled = true;

                if (Input.GetMouseButton(0))
                {
                    mainCamera.transform.position = Vector3.Slerp(balconyPos, roomPos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;
                    //mainCamera.transform.position = roomPos;
                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }
            }
            else
            {
                arrowToRoomFB.GetComponent<Behaviour>().enabled = false; 
            }
      
            if (hit.collider.gameObject.name == spirit.name)
            {
                spirit.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("spiritPressed");
                    spiritTwotextBox1.SetActive(true);
                }
            }
            else
            {
                spirit.GetComponent<Behaviour>().enabled = false; 
            }

            //exterior
            if (hit.collider.gameObject.name == arrowToBC.name)
            {
                arrowToBC.GetComponent<Behaviour>().enabled = true;

                if (Input.GetMouseButton(0))
                {
                    Debug.Log("arrowToBCPressed");
                    mainCamera.transform.position = Vector3.Slerp(ADpos, BCpos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;
                    //mainCamera.transform.position = BCpos;
                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }
            }
            else
            {
                arrowToBC.GetComponent<Behaviour>().enabled = false; 
            }
            if (hit.collider.gameObject.name == tree1.name)
            {
                tree1.GetComponent<Behaviour>().enabled = true;

                if (Input.GetMouseButton(0))
                {
                    treeRootTextbox.SetActive(true);

                }
            }
            else
            {
                tree1.GetComponent<Behaviour>().enabled = false; 
            }
            if (hit.collider.gameObject.name == tree2.name)
            {
                tree2.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    //treeRootTextbox.SetActive(true);

                }
                else
                {
                    tree2.GetComponent<Behaviour>().enabled = false; 
                }

            }
            if (hit.collider.gameObject.name == tree3.name)
            {
                tree3.GetComponent<Behaviour>().enabled = true;

                if (Input.GetMouseButton(0))
                {
                    //treeRootTextbox.SetActive(true);

                }

            }
            else
            {
                tree3.GetComponent<Behaviour>().enabled = false; 
            }
            if (hit.collider.gameObject.name == treeInside.name)
            {
                treeInside.GetComponent<Behaviour>().enabled = true;

                if (Input.GetMouseButton(0))
                {
                    //treeRootTextbox.SetActive(true);

                }

            }
            else
            {
                treeInside.GetComponent<Behaviour>().enabled = false; 
            }


            //lobby
            if (hit.collider.gameObject.name == arrowToExterior.name)
            {
                arrowToExterior.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    mainCamera.transform.position = Vector3.Slerp(lobbyPos, exteriorPos, transitionSpeed);
                    transitionSpeed += Time.deltaTime;
                    //mainCamera.transform.position = exteriorPos;

                    if (!newArea.isPlaying)
                    {
                        newArea.Play();
                    }
                }
            }
            else
            {
                arrowToExterior.GetComponent<Behaviour>().enabled = false; 
            }

            if (hit.collider.gameObject.name == spiritOne.name)
            {
                spiritOne.GetComponent<Behaviour>().enabled = true;
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("spiritOnePressed");
                    spiritOnetextBox1.SetActive(true);
                }
            }
            else
            {
                spiritOne.GetComponent<Behaviour>().enabled = false;
            }

            if (hit.collider.gameObject.name == hourGlass.name)
            {
                Debug.Log("hourGlassOver");
                hourGlass.GetComponent<Behaviour>().enabled = true;

                if (Input.GetMouseButton(0))
                {
                    hourglassTextBox.SetActive(true);


                }

            }
            else
            {
                hourGlass.GetComponent<Behaviour>().enabled = false;
            }
         
        }
     
        if (Input.GetMouseButton(0))
        {
            if (!click.isPlaying)
            {
                click.Play();
            }
        }

    }





}

