﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class sceneSwitch : MonoBehaviour {

    public string Title, Outdoors;
    Scene currentScene;
    public EventSystem eventSystems;
    public AudioSource bgm1;
    public AudioSource bgm2;
    public AudioSource startSound;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "Title")
        {
            if (!bgm1.isPlaying)
            {
                bgm1.Play();
            }
            if (Input.GetMouseButton(0))
            {
                if (!startSound.isPlaying)
                {
                    startSound.Play();
                }
                SceneManager.LoadScene("Outdoors");
            }
        }
        if (currentScene.name == "Outdoors")
        {
            if (!bgm2.isPlaying)
            {
                bgm2.Play();
            }
        }
	}
}
